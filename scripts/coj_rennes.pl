#!/usr/bin/perl
# <!-- coding: utf-8 -->
# coj_rennes.pl
# auteur: Marc GAUTHIER
# script pour coj rennes
#

use utf8;
use strict;
use warnings;
use Carp;
use Data::Dumper;
# use Data::Hexdumper qw(hexdump);
use Storable;
use Encode;
use MIME::Base64;
use File::Copy;
use File::Path;
use HTML::TableExtract;
use List::Util qw[min max];
use English;
#use Data::Dump qw(dump);
use Cwd;
use lib "../win32";
use lib "../../win32";
use lib "../win32/scripts";
use lib "../../win32/scripts";
use lib "../geo/scripts";
use lib "../../geo/scripts";
use lib "scripts";
use Utils;
use Utils8;
use Text::ParseWords;
our $baseDir = getcwd;
our $Drive = substr($baseDir,0,2);
  select (STDERR);$|=1;
  select (STDOUT);$|=1;
  binmode STDOUT, ":utf8";  # assuming your terminal is UTF-8
  binmode STDERR, ":utf8";  # assuming your terminal is UTF-8
  $baseDir =~ s{/scripts}{};
  chdir($baseDir);
#
#
our $dir = "$baseDir/../pub/coj/";
our $sqlDir = "${Drive}/bvi35/CouchesCojRennes2016";
$sqlDir = "${Drive}/web/bv/coj_rennes";
our $DEBUG = 0;
our (%especes, %admid);
&init_especes;
#
# ...
my $sp = 'extraitSql';
	if ( @ARGV ) {
		$sp = shift;
  }
  warn "Perl:$] $0 \$sp=$sp";
  for my $_sp ( split(/;/,$sp)  ) {
    my $sub = UNIVERSAL::can('main',"$_sp");
    if ( defined $sub ) {
      &$sub(@ARGV);
    } else {
      warn "main sp:$sp inconnu";
    }
  }
  exit;
#
# extraction de certains champs d'un tableau
sub extraitSql {
  &maille_lire;
  our %noms;
  my $f_sql = "${Drive}/web/bv/outils/adminer.sql";
  $f_sql = "${Drive}/web/bv/outils/dump_bvcoj.sql";
  $f_sql = "${Drive}/web/bv/outils/adminer_bvcoj.sql";
  warn "extraitSql() f_sql: $f_sql";
  my $f_mark = "${sqlDir}/maille_comptage.md";
  open(SQL, '< :utf8', $f_sql ) or die "extraitSql() erreur:$! open(SQL, $f_sql )";
  my (@champs, $champs, %champs, @valeurs, $stat, $mailles);
  my $geocode = "admid;sainom;saimai;credat;saityp;saicom;sailie;saiadr;sailon;sailat";
  while (my $ligne = <SQL> ) {
    chomp $ligne;
    if ( $ligne =~ m{INSERT INTO.*\((.*)\)} ) {
      my $champs = $1;
      $champs =~ s{[`\s]*}{}g;
      @champs = split(/,/, $champs);
#      confess Dumper \@champs;
      my $i = 0; %champs = map { $_ => $i++ } @champs;
      next;
    }
    if ( ! @champs ) {
      next;
    }
    if ( $ligne !~ m{^\(} ) {
      next;
    }
    $ligne =~ s{^\(}{};
    $ligne =~ s{\),*$}{};
    $ligne =~ s{\\[rn]}{ }g;
    @valeurs = quotewords(',', 0, $ligne);
    @valeurs = map { $_ =~ s/^\s*//; $_ } @valeurs;
    @valeurs = map { $_ =~ s/\\//g; $_ } @valeurs;
    $ligne = '';
    my $code_insee = $valeurs[$champs{'saicom'}];
    if ( ! $code_insee ) {
      next;
      warn "saicom " . $champs{'saicom'} . "=>" . $champs[10] . "=>" . $valeurs[10];
#      confess Dumper \@champs;
      confess Dumper \@valeurs;
    }
#    confess Dumper \@valeurs;
    if ( $code_insee ne '35 238' ) {
      next;
    }
    my $admid = $valeurs[$champs{'admid'}];
    my $maille = 0;
    if ( not defined $admid{$admid} ) {
      warn "extraitSQL() admid $admid";
#      next;
    } else {
      $maille = $admid{$admid};
    }
    $geocode .= sprintf("\n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s"
      , $valeurs[$champs{'admid'}]
      , $valeurs[$champs{'sainom'}]
      , $valeurs[$champs{'saimai'}]
      , $valeurs[$champs{'credat'}]
      , $valeurs[$champs{'saityp'}]
      , $valeurs[$champs{'saicom'}]
      , $valeurs[$champs{'sailie'}]
      , $valeurs[$champs{'saiadr'}]
      , $valeurs[$champs{'sailon'}]
      , $valeurs[$champs{'sailat'}]
    );
    $ligne .= sprintf("%s, %s, %s, %s %s\n"
      , $valeurs[$champs{'admid'}]
      , $valeurs[$champs{'sainom'}]
      , $valeurs[$champs{'saiadr'}]
      , $valeurs[$champs{'saidat'}]
      , $valeurs[$champs{'saiheu'}]
    );
    $ligne .= sprintf("%s\n"
      , $valeurs[$champs{'saitxt'}]
    );
    if ( $maille == 0 ) {
      warn "extraitSql() maille *** $ligne";
      next;
    }
    my $output = '';
    my $nb_especes = 0;
    my $nb_oiseaux = 0;
    $noms{$valeurs[$champs{'sainom'}]}->{'nb_comptages'}++;
    if ( $maille != 26 ) {
#      next;
    }
    $stat->{maille}->{$maille}->{nb_comptages}++;
    for my $key ( sort keys %especes) {
      if ( not defined $champs{$key} ) {
        warn "key:$key";
        confess Dumper \%champs;
      }
      my $nb = $valeurs[$champs{$key}];
      $nb =~ s{^>}{};
      if ( $nb =~ m{^\s*$} ) {
        next;
      }
      if ( $nb !~ m{^\d+$} ) {
        warn "extraitSql() nb:$nb *** $ligne";
        next;
      }
      if ( $nb == 0 ) {
#        warn $ligne;
        next;
      }
      my $espece = $especes{$key};
      $nb_especes++;
      $nb_oiseaux += $nb;
      $stat->{espece}->{$espece}->{nb_comptages}++;
      $stat->{espece}->{$espece}->{nb_oiseaux} += $nb;
      $stat->{maille}->{$maille}->{nb_oiseaux} += $nb;
      if ( not defined $stat->{maille}->{$maille}->{espece}->{$espece} ) {
        $stat->{maille}->{$maille}->{nb_especes}++;
        $stat->{maille}->{$maille}->{espece}->{$espece} = $nb;
      }
      $stat->{maille}->{$maille}->{espece}->{$espece} = max($stat->{maille}->{$maille}->{espece}->{$espece}, $nb);
      $output .= sprintf(" %s (%d)", $espece, $nb);
    }
    for (my $i = 1; $i <= 8; $i++) {
      my $nb = $valeurs[$champs{"nb$i"}];
      if ( $nb !~ m{^\d+$} ) {
        next;
      }
      if ( $nb < 1 ) {
        next;
      }
      my $espece = $valeurs[$champs{"esp$i"}];
      $nb_especes++;
      $nb_oiseaux += $nb;
      $stat->{espece}->{$espece}->{nb_comptages}++;
      $stat->{espece}->{$espece}->{nb_oiseaux} += $nb;
      $stat->{maille}->{$maille}->{nb_oiseaux} += $nb;
      if ( not defined $stat->{maille}->{$maille}->{espece}->{$espece} ) {
        $stat->{maille}->{$maille}->{nb_especes}++;
        $stat->{maille}->{$maille}->{espece}->{$espece} = $nb;
      }
      $stat->{maille}->{$maille}->{espece}->{$espece} = max($stat->{maille}->{$maille}->{espece}->{$espece}, $nb);
      $output .= sprintf(" %s (%d)", $espece, $nb);
    }
    $output =~ s{^\s+}{};
    $ligne .= "\n$output\n";
    $ligne .= "\nnb_especes: $nb_especes nb_oiseaux: $nb_oiseaux\n";
    push @{$mailles->{$maille}}, $ligne;
  }
  close(SQL);
  putLignesUTF8("$sqlDir/geocode.csv", $geocode);
#  print Dumper \%noms;
  my $csv_nom = sprintf("%s\t%s\t%s\n", "nom", "bvi35", "nb_comptages");
  for my $nom ( sort keys %noms ) {
    my $bvi35 = 0;
    if ( $noms{$nom}->{'nb_comptages'} > 1 ) {
#      $bvi35 = 1;
    }
    if ( $nom =~ m{Bastien}
      || $nom =~ m{Beaufils}
      || $nom =~ m{Gauthier}
      || $nom =~ m{Griffaut}
      || $nom =~ m{Margot Brunelli}i
      || $nom =~ m{Mathis}i
      || $nom =~ m{Melaine}
    ) {
      $bvi35 = 1;
    }

    $csv_nom .= sprintf("%s\t%s\t%s\n", $nom, $bvi35, $noms{$nom}->{'nb_comptages'});
  }
  putLignesUTF8("$sqlDir/nom_stat.csv",$csv_nom);
#  confess Dumper $stat->{maille};
  my $csv = sprintf("%s\t%s\t%s\t%s\t%s\n", 'maille'
    , 'nb_comptages'
    , 'nb_especes'
    , 'nb_comptes'
    , 'nb_oiseaux'
  );
  my $csv_mark = sprintf("|%s|%s|%s|%s|%s|\n|:------:|:-----:|:------:|:------:|:------:|\n", 'maille'
    , 'nb_comptages'
    , 'nb_especes'
    , 'nb_comptes'
    , 'nb_oiseaux'
  );
  for my $maille ( sort { $a <=> $b } keys %{$stat->{maille}} ) {
    my $nb_especes = 0;
    my $nb_oiseaux = 0;
#    confess Dumper $stat->{maille}->{$maille};
    my $output = '';
    for my $espece ( sort keys %{$stat->{maille}->{$maille}->{espece}} ) {
      $nb_especes++;
      $nb_oiseaux += $stat->{maille}->{$maille}->{espece}->{$espece};
      $output .= sprintf(" %s (%d)", $espece, $stat->{maille}->{$maille}->{espece}->{$espece});
    }
    $output =~ s{^\s+}{};
    my $ligne = "\n*********************\n$output\n";
    $ligne .= "\nnb_especes: $nb_especes nb_oiseaux: $nb_oiseaux\n";
    if ( $stat->{maille}->{$maille}->{nb_comptages} > 1 ) {
      push @{$mailles->{$maille}}, $ligne;
    }
    if ( $nb_especes >= 18 ) {
#      confess Dumper $stat;
    }
    $csv .= sprintf("%s\t%s\t%s\t%s\t%s\n", $maille
      , $stat->{maille}->{$maille}->{nb_comptages}
      , $stat->{maille}->{$maille}->{nb_especes}
      , $stat->{maille}->{$maille}->{nb_oiseaux}
      , $nb_oiseaux
    );
    $csv_mark .= sprintf("|%s|%s|%s|%s|%s|\n", $maille
      , $stat->{maille}->{$maille}->{nb_comptages}
      , $stat->{maille}->{$maille}->{nb_especes}
      , $stat->{maille}->{$maille}->{nb_oiseaux}
      , $nb_oiseaux
    );
  }
  print $csv;
  putLignesUTF8("$sqlDir/maille_stat.csv",$csv);
  my $mark = <<EOF;
---
title: "Les comptages par maille"
author: "Marc Gauthier"
output: pdf_document
---
${csv_mark}

EOF
  for my $maille ( sort { $a <=> $b } keys %{$mailles} ) {
    $mark .= "\n## maille $maille\n";
    $mark .= join("\n", @{$mailles->{$maille}});
  }
  putLignesUTF8($f_mark, $mark);
}
#
sub init_especes {
  %especes = (
    'accmou' => 'Accenteur mouchet',
    'bergri' => 'Bergeronnette grise',
    'boupiv' => 'Bouvreuil pivoine',
    'chaele' => 'Chardonneret élégant',
    'chotou' => 'Choucas des tours',
    'cornoi' => 'Corneille noire',
    'etosan' => 'Etourneau sansonnet',
    'faunoi' => 'Fauvette à tête noire',
    'geache' => 'Geai des chênes',
    'grimau' => 'Grive mauvis',
    'grimus' => 'Grive musicienne',
    'mernoi' => 'Merle noir',
    'mesble' => 'Mésange bleue',
    'mescha' => 'Mésange charbonnière',
    'meslon' => 'Mésange à longue queue',
    'mesnon' => 'Mésange nonnette',
    'moidom' => 'Moineau domestique',
    'picepe' => 'Pic épeiche',
    'picver' => 'Pic vert',
    'piebav' => 'Pie bavarde',
    'pinarb' => 'Pinson des arbres',
    'pinnor' => 'Pinson du nord',
    'pigram' => 'Pigeon ramier',
    'rougor' => 'Rouge-gorge familier',
    'sittor' => 'Sittelle torchepot',
    'taraul' => 'Tarin des aulnes',
    'toutur' => 'Tourterelle turque',
    'tromig' => 'Troglodyte mignon',
    'vereur' => "Verdier d'Europe",
  );
}
sub _pointIsInPolygon {
  my ($a_point, $n, $a_x, $a_y) = @_;

  # point coords
  my ($x, $y) = ($a_point->[0], $a_point->[1]);

  # poly coords
  # $n is the number of points in polygon.
  my @x = @$a_x; # Even indices: x-coordinates.
  my @y = @$a_y; # Odd indices: y-coordinates.

  my ($i, $j);                          # Indices.
  my $side = 0;                         # 0 = outside, 1 = inside.
  for ($i = 0, $j = $n - 1 ; $i < $n; $j = $i++) {
    if (
        (
          # If the y is between the (y-) borders ...
          (($y[$i] <= $y) && ($y < $y[$j])) ||
          (($y[$j] <= $y) && ($y < $y[$i]))
        )
        and
          # ...the (x,y) to infinity line crosses the edge
          # from the ith point to the jth point...
          ($x
            <
            ($x[$j] - $x[$i] ) *
            ($y - $y[$i]) / ($y[$j] - $y[$i]) + $x[$i] )) {
         $side = not $side; # Jump the fence.
    }
  }
  return $side ? 1 : 0;
}
sub maille_lire {
  my $f_csv = "${Drive}/bvi35/CouchesRennes2016/admid_lonlat.csv";
  $f_csv = "${Drive}/web/bv/coj_rennes/admid_lonlat.csv";
  open(CSV, '< :utf8', $f_csv ) or die "maille_lire() erreur:$! open(CSV, $f_csv )";
  my ( @valeurs, @champs, %champs);
  while (my $ligne = <CSV> ) {
    chomp $ligne;
    @valeurs = split('\t', $ligne);
    if ( ! @champs ) {
      @champs = @valeurs;
      my $i = 0; %champs = map { $_ => $i++ } @champs;
#      confess Dumper \@champs;
      next;
    }
    my $admid = $valeurs[$champs{'admid'}];
    my $maille = $valeurs[$champs{'maille'}];
    $admid{$admid} = $maille;
  }
}