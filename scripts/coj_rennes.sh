#!/bin/sh
# <!-- coding: utf-8 -->
#T coj_rennes
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/misc_pkg.sh ] && . ../win32/scripts/misc_pkg.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  ENV
  CFG="COJ_RENNES"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  LOG "CONF fin"
}

#F e: edition des principaux fichiers
e() {
  LOG "e debut"
  E scripts/coj_rennes.sh
  E scripts/coj_rennes.R
  E scripts/coj_rennes.pl
  LOG "e fin"
}
#f wm:
wm() {
  LOG "wm debut"
  ( cd /d/web/bv ; explorer . &)
  ( cd /d/web.heb/bv ; explorer . &)
  WM d:/web/bv d:/web.heb/bv &
  LOG "wm fin"
}
#f MARK2HTML:
MARK2HTML() {
  LOG "MARK2HTML debut"
  /c/Users/GXHZ4242/AppData/Local/Pandoc/pandoc.exe  -f markdown+pipe_tables -t html -s -o maille_comptage.html /c/bvi35/CouchesCojRennes2016/maille_comptage.md
  LOG "MARK2HTML fin"
}
#f MARK2TEX:
MARK2TEX() {
  LOG "MARK2TEX debut"
  _ENV_tex
  /c/Users/GXHZ4242/AppData/Local/Pandoc/pandoc.exe  -f markdown -t latex --template="scripts/coj_rennes.latex" -s -o maille_comptage9.pdf /c/bvi35/CouchesCojRennes2016/maille_comptage.md
  LOG "MARK2TEX fin"
}
#f MARK2DOCX:
MARK2DOCX() {
  LOG "MARK2DOCX debut"
  /c/Users/GXHZ4242/AppData/Local/Pandoc/pandoc.exe  -f markdown+pipe_tables -o maille_comptage.docx /c/bvi35/CouchesCojRennes2016/maille_comptage.md
  LOG "MARK2DOCX fin"
}
#f IRIS:
IRIS() {
  LOG "IRIS debut"
  _ENV_gdal210
  rm /d/bvi35/CouchesIGN/IRIS35238.kml
  f_shp=/d/bvi35/CouchesIGN/CONTOURS-IRIS_2-0__SHP_LAMB93_FXX_2014-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2014/CONTOURS-IRIS_2-0_SHP_LAMB93_D035-2014/CONTOURS-IRIS_D035.shp
  ogr2ogr -f "kml" -overwrite -skipfailures /d/bvi35/CouchesIGN/IRIS35238.kml $f_shp -where "'NOM_COM' LIKE 'Rennes%'" -lco ENCODING=LATIN1
#  E /d/bvi35/CouchesIGN/IRIS35238.kml
  LOG "IRIS fin"
}
#f T:
T() {
  LOG "T debut"
  perl scripts/coj_rennes.pl
  MARK2DOCX
# E maille_comptage.html
  LOG "T fin"
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  Local="${DRIVE}/web/geo";  Depot=coj_rennes; Remote=frama
  export Local
  export Depot
  export Remote
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
#  bash ../win32/scripts/git.sh PUSH
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  cat  <<'EOF' > /tmp/git.lst
scripts/coj_rennes.sh
EOF
  ls -1 scripts/coj_rennes*.pl >> /tmp/git.lst
  ls -1 scripts/coj_rennes*.R >> /tmp/git.lst
  ls -1 ${CFG}/*.tex >> /tmp/git.lst
  cat  <<'EOF' > /tmp/README.md
# coj_rennes : Comptage Oiseaux de Jardins sur Rennes en 2016

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases coj.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier COJ_RENNES.

EOF
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done